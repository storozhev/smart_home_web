mod db;

use actix_web::{
    http::StatusCode,
    web::{self},
    App, HttpResponse, HttpServer, Responder,
};
use db::{
    models::{Device, NewDevice, NewRoom, Room},
    *,
};
use serde::Serialize;
use std::sync::Mutex;

#[derive(Serialize)]
struct SmartHome {
    name: String,
    rooms: Mutex<Vec<(Room, Device)>>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(SmartHome {
                name: "my_smart_home".into(),
                rooms: Mutex::new(vec![]),
            }))
            .service(
                web::scope("/api")
                    .service(
                        web::scope("/room")
                            .route("", web::post().to(create_room_handler))
                            .service(
                                web::scope("/{room_id}")
                                    .route("", web::delete().to(remove_room_handler))
                                    .route("/device", web::post().to(create_device_handler))
                                    .route(
                                        "/device/{device_id}",
                                        web::delete().to(remove_device_handler),
                                    ),
                            ),
                    )
                    .service(web::resource("/info").route(web::get().to(info_handler))),
            )
    })
    .bind(("127.0.0.1", 8000))?
    .run()
    .await
}

#[derive(Serialize)]
struct SuccessResponse {}

#[derive(Serialize)]
struct FailResponse {
    error: String,
}

async fn create_room_handler(raw_name: web::Bytes) -> impl Responder {
    let name = String::from_utf8(raw_name.to_vec()).unwrap();
    match insert_room(NewRoom { name: &name }) {
        Ok(_) => HttpResponse::Ok().json(SuccessResponse {}),
        Err(e) => {
            eprintln!("failed to create room: {}", e);
            HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).json(FailResponse {
                error: e.to_string(),
            })
        }
    }
}

async fn create_device_handler(raw_name: web::Bytes, room_id: web::Path<i32>) -> impl Responder {
    match insert_device(NewDevice {
        name: String::from_utf8(raw_name.to_vec()).unwrap().as_str(),
        room_id: &room_id.into_inner(),
    }) {
        Ok(_) => HttpResponse::Ok().json(SuccessResponse {}),
        Err(e) => {
            eprintln!("failed to create device: {}", e);
            HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).json(FailResponse {
                error: e.to_string(),
            })
        }
    }
}

async fn remove_room_handler(room_id: web::Path<i32>) -> impl Responder {
    match remove_room(room_id.into_inner()) {
        Ok(_) => HttpResponse::Ok().json(SuccessResponse {}),
        Err(e) => {
            eprintln!("failed to remove room: {}", e);
            HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).json(FailResponse {
                error: e.to_string(),
            })
        }
    }
}

async fn remove_device_handler(params: web::Path<(i32, i32)>) -> impl Responder {
    let (room_id, device_id) = params.into_inner();
    match remove_device(room_id, device_id) {
        Ok(_) => HttpResponse::Ok().json(SuccessResponse {}),
        Err(e) => {
            eprintln!("failed to remove device: {}", e);
            HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).json(FailResponse {
                error: e.to_string(),
            })
        }
    }
}

async fn info_handler(smart_home: web::Data<SmartHome>) -> impl Responder {
    match get_rooms() {
        Ok(result) => {
            let smart_home = smart_home.into_inner();
            *smart_home.rooms.lock().unwrap() = result;
            HttpResponse::Ok().json(smart_home.as_ref())
        }
        Err(e) => {
            eprintln!("failed to get rooms: {}", e);
            HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).json(FailResponse {
                error: e.to_string(),
            })
        }
    }
}
