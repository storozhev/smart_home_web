pub mod models;
pub mod schema;

use diesel::sqlite::SqliteConnection;
use dotenv::dotenv;
use std::env;
use std::error::Error;

use self::schema::*;
use diesel::prelude::*;
use models::*;

fn establish_connection() -> SqliteConnection {
    dotenv().ok();
    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL env variable was not found");
    SqliteConnection::establish(&db_url).unwrap_or_else(|e| {
        panic!("failed to connect to {} error: {}", db_url, e);
    })
}

pub fn get_rooms() -> Result<Vec<(Room, Device)>, Box<dyn Error>> {
    let conn = &mut establish_connection();
    let data: Vec<(Room, Device)> = rooms::dsl::rooms
        .inner_join(devices::dsl::devices)
        .load(conn)?;

    Ok(data)
}

pub fn insert_room(room: NewRoom) -> Result<(), Box<dyn Error>> {
    let conn = &mut establish_connection();
    diesel::insert_into(rooms::dsl::rooms)
        .values(room)
        .execute(conn)?;

    Ok(())
}

pub fn insert_device(device: NewDevice) -> Result<(), Box<dyn Error>> {
    let conn = &mut establish_connection();
    diesel::insert_into(devices::dsl::devices)
        .values(device)
        .execute(conn)?;

    Ok(())
}

pub fn remove_room(room_id: i32) -> Result<(), Box<dyn Error>> {
    let conn = &mut establish_connection();
    diesel::delete(rooms::dsl::rooms)
        .filter(rooms::dsl::id.eq(room_id))
        .execute(conn)?;

    Ok(())
}

pub fn remove_device(room_id: i32, device_id: i32) -> Result<(), Box<dyn Error>> {
    let conn = &mut establish_connection();
    diesel::delete(devices::dsl::devices)
        .filter(devices::dsl::room_id.eq(room_id))
        .filter(devices::dsl::id.eq(device_id))
        .execute(conn)?;

    Ok(())
}
