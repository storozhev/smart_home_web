use super::schema::devices;
use super::schema::rooms;
use diesel::Identifiable;
use diesel::{Associations, Insertable, Queryable};
use serde::Serialize;

#[derive(Queryable, Serialize, Identifiable)]
pub struct Room {
    id: i32,
    name: String,
}

#[derive(Insertable)]
#[diesel(table_name = rooms)]
pub struct NewRoom<'a> {
    pub name: &'a str,
}

#[derive(Queryable, Serialize, Identifiable, Associations)]
#[diesel(belongs_to(Room))]
pub struct Device {
    id: i32,
    name: String,
    room_id: i32,
}

#[derive(Insertable)]
#[diesel(table_name = devices)]
pub struct NewDevice<'a> {
    pub name: &'a str,
    pub room_id: &'a i32,
}
