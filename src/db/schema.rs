// @generated automatically by Diesel CLI.

diesel::table! {
    devices (id) {
        id -> Integer,
        name -> Text,
        room_id -> Integer,
    }
}

diesel::table! {
    rooms (id) {
        id -> Integer,
        name -> Text,
    }
}

diesel::joinable!(devices -> rooms (room_id));

diesel::allow_tables_to_appear_in_same_query!(devices, rooms,);
