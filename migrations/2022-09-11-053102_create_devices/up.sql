-- Your SQL goes here
create table if not exists devices (
    id integer not null primary key,
    name varchar not null,
    room_id integer not null,
    FOREIGN KEY (room_id) REFERENCES rooms(id)
)